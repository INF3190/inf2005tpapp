/*Author: Johnny Tsheke @ INF2005 -- UQAM
 *Cette petite application node.js sert a demarrer un serveur web local
 *pour interagir avec les page web (html, css, js, etc) des TP du cours INF2005 @UQAM 
 *
 */
var express = require('express');
var app=express();
var bodyParser = require('body-parser');// pour les formulaires avec method post
// si ca ne marche pas installez express avec la commade suivante:
// npm install express body-parser--save



//remplacez la valeur de htdocs par le chemin du dossier contenant 
//vos fichiers html

//var htdocs= __dirname+'public_html';//pour chemin absolu ATT au commutateur: 
 var htdocs='public_html';
console.log(htdocs);
//app.use(express.static(__dirname +'public_html'));
app.use(express.static(htdocs));
var inf2005server='inf2005server';
console.log(inf2005server);
app.use(express.static(inf2005server)) // charger après pour donner priorité aux page de l'utilisateur

//ajout quelques routes
//connect
app.get('/inf2005connect', function(reg,res){
	
	var text='<h3 style="color:red;">Utilisez la methode post pour la connexion</h3>';
	text=text+'<br/><a href="index.html">Retour à la page d\'acceuil</a>';
	res.send(text);
});

app.post('/inf2005connect', function (req, res) {
	
	var text='<h3 style="color:blue;">Connecté(e) !</h3>';
	text=text+'<br/><a href="index.html">Retour à la page d\'acceuil</a>';
	res.send(text);
	});

//contacts

app.get('/inf2005contacts', function(reg,res){
	
	var text='<h3 style="color:red;">Utilisez la methode post pour le formulaire  contacts</h3>';
	text=text+'<br/><a href="index.html">Retour à la page d\'acceuil</a>';
	res.send(text);
});

app.post('/inf2005contacts', function (req, res) {
	
	var text='<h3 style="color:blue;">Merci. Nous avons reçu votre demande de contact </h3>';
	text=text+'<br/><a href="index.html">Retour à la page d\'acceuil</a>';
	res.send(text);
	});


// fin routes
var portNumber=3000;// au besoin changez le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');
//pour executer, tapez la commade suivante:
//node inf2005.js
//Avec un navigateur web, visitez le site http://localhost:3000
//Au besoin remplacez 81 par le numero de port utilisé dans la variable portNumber
