Application pour faire les TP du cours INF2005 à l'UQAM. 

Cette application nécessite node.js (https://nodejs.org/)

Utilisation

------------

Télécharger  l'application puis la décompresser ou la clonner avec git:

git clone https://gitlab.com/INF2005/inf2005tpapp.git

Par la suite, en utilisant un terminal, aller  dans le dossier principal de l'application et tapez la commande suivante:

node inf2005.js

Si tout va bien, un numéro de port s'affichera (ex: 3000)

Si tout se passe bien, alors vous pouvez ouvir un navigateur (fureteur) web puis visiter l'url suivant:

http://localhost:3000/inf2005tpapp.html

Si vous voyez une page s'affichez c'est que tout fonctionne bien. Vous pouvez alors commencer à développer votre site dans le dossier public_html. N'oubliez pas que la page visible par défaut est index.html. Vous avec donc besoin d'avoir un fichier index.html dans le dossier public_html.

-----------------------------------------

BOOTSTRAP



Le sous dossier  public_html/css contient déjà le fichier bootstrap.min.css  que vous pourriez utiliser. Ce fichier provient du site http://getbootstrap.com

ATTENTION: Ce dossier public_html ne doit pas être renommer!


-------------------------------------------

GESTION DE CODE AVEC GIT

Les explications de cette partie s'addressent à ceux qui veulent utiliser git pour la gestion de leur code source.

Prérequis: Installation nodejs (https://nodejs.org/) et git (https://git-scm.com/downloads).

Pour cette cession d'automne 2016, nous utilisons nodejs V4.6.0 LTS

Pour vous permettre de mettre à jour l'application inf2005tpapp de temps en temps et aussi de continuer à gérer le code sur votre propre dépôt git privé, nous vous recommandons de clonner l'application et de développer vos fichiers dans le dossier public_html selons les indications de l'énoncé. Pour cela, vous condérons que vous voulez héberger votre dépôt sur gitlab (la procédure est aussi valable pour d'autres fournisseurs git). Voici les étapes à suivre.

1. Se créer un compte sur https://gitlab.com si ce n'est pas encore fait

2. Créez un projet (inf2005tpapp) git privé et copiez l'url https. Supposons que cet url est le suivant:

 https://gitlab.com/etudiant/inf2005tpapp.git

3. Sur votre machine locale ouvrez un terminal (ou in terminal git-bash sous Windows) puis placez-vous dans le dossier où vous voulez mettre votre application inf2005tpapp

4. Clonez l'application inf2005tpapp avec la commande suivante (Nécessite une connexion Internet au moement de l'exécution de la commande):

git clone -o inf2005 https://gitlab.com/INF2005/inf2005tpapp.git

Si tout se passe bien, l'application sera téléchargée dans le dossier inf2005tpapp

5. Créez un remote (une connexion) vers votre propre dépôt git

 A l'aide d'un terminal, allez dans le dossier inf2005tpapp:
 
 cd inf2005tpapp
 
 Tapez la commande suivante:
 
 git remote add origin https://gitlab.com/etudiant/inf2005tpapp.git
 
 
 Pour vérifier que votre remote s'est bien ajouté, tapez la commande suivante:
 
 git remote -v
 
 6. Poussez l'application vers votre dépôt privé:
 
 git push origin master
 
 A la demande entrez le login et le mot de passe. Si tout se passe bien alors vous pouvez vérifier votre dépôt avec un navigateur web et vous verrez les fichiers de l'application
 
 7. Ajoutez/Modifiez vos pages et poussez vers votre dépôt git
 
 Précison tout de suite que certaines de ces manipulations peuvent se faire via l'interface graphique sur eclipse.
 
 Après ajout et/ou modification des fichiers (dans public_html), taper les commandes suivantes:
 
 git add public_html
 
 git commit -m "vos commentaires sur cet enregistrement"
 
 git push origin master
 
 
 8. Metre à jour l'application:
 
 
 git pull inf2005 master
 
 (s'il y a des maj, il faudra les pousser vers votre dépôt git)
 
 git pull origin master
 
 ==========================
 
 # Quelques librairies
 
 Quelquques librairie gratuites sont insérées dans l'application et disponible das le sous dossier public/node_modules. Plusieurs librairies ont jQuery comme prérequis. On vous suggère de charger la librairie jquery une seule fois avant les autres librairie:
 
 public_html/node_modules/jquery/dist/jquery.min.js
 
 Les librairies suivantes sont disponibles dans public_html/node_modules
 
 
 - jQuery
 - bootstrap
 - datatables
 - jstree
 
 
 


